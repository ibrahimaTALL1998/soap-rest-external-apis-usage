package sn.dgi.esp.ApplicationCliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationClienteApplication.class, args);
	}

}

package sn.dgi.esp.ApplicationCliente.Controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import sn.dgi.esp.ApplicationCliente.domain.Role;
import sn.dgi.esp.ApplicationCliente.domain.User;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
@RequestMapping("/web/user")
public class HomeController {

    // @RequestMapping("/api")
    @GetMapping("/hello")
    public String Hello() {
        return "Hello World";
    }

    // @RequestMapping("/api/show")
    @GetMapping()
    public String showAll(Model model) {
        String uri = "http://localhost:8900/api/user";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);
        // return result;
        model.addAttribute("users", result);
        return "user/all";
    }

    // @RequestMapping("/api/single")
    @GetMapping("/{id}")
    public String showOne(Model model, @RequestBody int id) {
        String uri = "http://localhost:8900/api/user/" + id;
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);
        // return result;
        model.addAttribute("users", result);
        return "user/details";
    }

    // @RequestMapping("/api/create")
    @PostMapping()
    public String create(@RequestBody User user) {
        String uri = "http://localhost:8900/api/user";
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<User> requestBody = new HttpEntity<>(user);
        User e = restTemplate.postForObject(uri, requestBody, User.class);
        if (e != null && e.getUsername() != null) {

            System.out.println("User created: " + e.getUsername());
        } else {
            System.out.println("User error!");
        }

        return "redirect:/web/user";
    }

    @GetMapping("/delete/{id}")
    public String delete(@RequestBody Integer id) {
        // Integer id = user.getId();
        String uri = "http://localhost:8900/web/user/delete/" + id;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(uri);
        return "redirect:/web/user";
    }
}
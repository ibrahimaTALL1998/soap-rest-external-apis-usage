// package sn.dgi.esp.ApplicationCliente.Controller;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.ui.Model;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.ModelAttribute;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
// import sn.dgi.esp.ApplicationCliente.domain.*;

// @Controller
// @RequestMapping("/web/user")
// public class UserController {

//   @Autowired
//   private UserRepository userRepository;
//   @Autowired
//   private RoleRepository roleRepository;

//   @GetMapping()
//   public String all(Model model) {
//     model.addAttribute("users", userRepository.findAll());

//     return "user/all";
//   }

//   @GetMapping("/edit")
//   public String edit(
//     @RequestParam(value = "id", required = false) Integer id,
//     Model model
//   ) {
//     if (id != null) {
//       model.addAttribute("user", userRepository.findById(id).get());
//     } else {
//       model.addAttribute("user", new User());
//     }
//     model.addAttribute("roles", roleRepository.findAll());
//     return "user/edit";
//   }

//   @GetMapping("/delete/{id}")
//   public String delete(@ModelAttribute("id") Integer id) {
//     userRepository.deleteById(id);

//     return "redirect:/web/user";
//   }

//   @PostMapping()
//   public String save(@ModelAttribute("user") User user) {
//     this.userRepository.save(user);
//     return "redirect:/web/user";
//   }
// }
